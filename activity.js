// Aggregate to count the total numnber of items supplied by red farm
db.fruits.aggregate([

		{$match:{supplier:"Red Farms Inc"}},
		{$count:"numberSupplied"}


	])
// Aggregate to count the total numnber of items supplied by Green Farming
db.fruits.aggregate([

		{$match:{supplier:"Green Farming and Canning"}},
		{$count:"numberSupplied"}


	])


// Aggregate to get the average price of all fruits that are onSale per supper
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group: {_id:"$supplier", avgAmountPrice:{$avg:"$price"}}}


	])

// Aggregate to get the highest price of a fruit per supplier
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group: {_id:"$supplier", highestAmountPrice:{$max:"$price"}}}


	])


// Aggregate to get the lowest price of a fruit per supplier
db.fruits.aggregate([

		{$match:{onSale:true}},
		{$group: {_id:"$supplier", lowestAmountPrice:{$min:"$price"}}}


	])